import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String amountVal;
  String titleVal;

  List<Widget> homeBody = [];

  Widget addCard() {
    return Container(
      padding: EdgeInsets.only(top: 1, left: 5, right: 5),
      child: Card(
        elevation: 10,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.white)),
              child: Text(
                amountVal,
                style: TextStyle(fontSize: 20),
              ),
            ),
            Column(
              children: <Widget>[
                Text(titleVal),
                Text(
                    DateFormat('yyyy-MM-dd      hh-mm').format(DateTime.now())),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('.point.'),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * .785,
        child: ListView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: homeBody,
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 50, right: 30),
        child: FloatingActionButton(
            child: Icon(Icons.add, size: 30,),
            onPressed: () {
              showModalBottomSheet(
                  context: context,
                  builder: (_) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          TextField(
                            decoration: InputDecoration(
                              labelText: 'amount',
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                            onChanged: (val) {
                              amountVal = val;
                            },
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          TextField(
                            decoration: InputDecoration(
                              labelText: 'title',
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                            onChanged: (val) {
                              titleVal = val;
                            },
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          FlatButton(
                            onPressed: () {
                              setState(() {
                                homeBody.add(addCard());
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(top: 16, bottom: 16),
                              child: Text(
                                'Add Point',
                                style: TextStyle(color: Colors.black,fontSize: 20),
                              ),
                            ),
                            color: Colors.tealAccent,
                          ),
                        ],
                      ),
                    );
                  });
            }),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
